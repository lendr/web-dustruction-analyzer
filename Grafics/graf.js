$(document).ready(function () {

});

function addGroup() {
    var groupLink = document.getElementById("groupInput").value;
    if (groupLink == '') {
        alert("Заполните поле");
        return;
    }
    var row = document.getElementById("table").insertRow();
    var cellLinkl = row.insertCell();
    cellLinkl.innerHTML = groupLink;
    var cellButton = row.insertCell();
    cellButton.innerHTML = '<button class="btn btn-primary" onclick=deleteRow(this)>Delete</button>'
    document.getElementById("groupInput").value = '';
}
function deleteRow(button) {
    var row = button.parentNode.parentNode;
    row.parentNode.removeChild(row);
}

function start() {
    var table = document.getElementById("table");
    var body = [];
    for (var i = 1, row; row = table.rows[i]; i++) {
        var url = row.cells[0].textContent;
        body[i - 1] = url;

    }
    var request = new XMLHttpRequest();
    request.open('POST', 'http://localhost:8081/v1/parse');
    request.setRequestHeader('Content-Type', 'application/json');

    request.send(JSON.stringify(body));

    document.getElementById("inputDiv").style.display = 'none';
    document.getElementById("statistic").style.display = "block";
    setInterval(fetchStatistics, 600000);
    // createHighcharts();
}

function stop() {
    // document.getElementById("inputDiv").style.display = 'block';
    var request = new XMLHttpRequest();
    request.open('GET', 'http://localhost:8081/v1/stop');
    request.setRequestHeader('Content-Type', 'application/json');
    request.send();

}

function fetchStatistics() {
    var request = new XMLHttpRequest();
    request.open('GET', 'http://localhost:8081/v1/graphics/foundContents');
    request.setRequestHeader('Content-Type', 'application/json');
    request.onload = function () {
        var response = JSON.parse(request.response);
        var table = document.getElementById("tableBody");
        clearTable("tableBody");
        for (var i = 0; i < response.length; i++) {
            var row = table.insertRow();
            var url = row.insertCell();
            url.innerHTML = response[i].url;
            var dateFound = row.insertCell();
            dateFound.innerHTML = (new Date(response[i].foundDate)).toLocaleString();
            var categories = row.insertCell();
            categories.innerHTML = response[i].categories;
        }
    }
    request.send();
    fetchContentsByTime();
    fetchCategoriesHighCharts();
}

function clearTable(tableId) {
    var Parent = document.getElementById(tableId);
    while (Parent.hasChildNodes()) {
        Parent.removeChild(Parent.firstChild);
    }
}

function fetchCategoriesHighCharts(){
    var request = new XMLHttpRequest();
    request.open('GET', 'http://localhost:8081/v1/graphics/categories');
    request.setRequestHeader('Content-Type', 'application/json');
    request.onload = function () {
        var response = JSON.parse(request.response);
        Highcharts.chart('categoriesHighCharts',response);
    }
    request.send();
}

function fetchContentsByTime(){
    var request = new XMLHttpRequest();
    request.open('GET', 'http://localhost:8081/v1/graphics/contentsByTime');
    request.setRequestHeader('Content-Type', 'application/json');
    request.onload = function () {
        var response = JSON.parse(request.response);
        Highcharts.chart('TimeFound',response);
    }
    request.send();
}